# JAVA Terminal Project Noroff

Project that lets you do several operations in the terminal.
Example of operations in the program: Print files in folder, Print all files with a given file-extention, Search for words in a text file, Log all operations to a log file

## Operations needed for compiling and running in terminal:
-     cd into the project folder
-     javac -d out .\src\java*.java .\src\java\logging\*.java .\src\java\menus\*.java .\src\java\manipulation\*.java
-     cd out
-     jar cfe Program.jar Main *.class .\logging*.class .\menus*.class .\manipulation*.class
-     java -jar Program.jar


![terminal ](./Screenshot_task.PNG)

## Instalation

No instalation needed. Just clone project and run.

## Known bugs