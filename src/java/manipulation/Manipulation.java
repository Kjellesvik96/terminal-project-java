package manipulation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.Scanner;

import static logging.Logging.logging;
import static menus.Menus.displayManipulation;
import static menus.Menus.displayOptions;

public class Manipulation {

    /* Method to move the user in right direction regarding what
     * the user chooses from the displayManipulation method.
     */
    public static void fileManipulation() {
        Scanner scanner = new Scanner(System.in);
        boolean checker = false;
        int userInput;
        displayManipulation();

        while (!checker) {
            if (scanner.hasNextInt()) {
                userInput = scanner.nextInt();
                if (userInput <= 9) {
                    switch (userInput) {
                        case 1 -> {
                            System.out.println("Listing filenames");
                            listNames();
                            checker = true;
                        }
                        case 2 -> {
                            System.out.println("Getting files by extension");
                            getFiles();
                            checker = true;
                        }
                        case 3 -> {
                            System.out.println("Getting name of file");
                            getName();
                            checker = true;
                        }
                        case 4 -> {
                            System.out.println("Getting size of file");
                            getSize();
                            checker = true;
                        }
                        case 5 -> {
                            System.out.println("Getting number of lines");
                            getLines();
                            checker = true;
                        }
                        case 6 -> {
                            System.out.println("Heading to SearchPage");
                            search();
                            checker = true;
                        }
                        case 7 -> {
                            System.out.println("Heading to WordCounter");
                            countWord();
                            checker = true;
                        }
                        case 8 -> {
                            System.out.println("Heading back to main menu");
                            displayOptions();
                            checker = false;
                        }
                        case 9 -> {
                            System.out.println("Exiting program...");
                            Date date = new Date();
                            long elapsedTime = 0;
                            String action = "SESSION EXITED";
                            logging(date, elapsedTime, action);
                            System.exit(0);
                            checker = true;
                        }
                        default -> System.out.println("ILLEGAL INPUT: " + userInput);
                    }

                } else {
                    System.out.println("Your input is not valid, enter a number between 1-3");
                }
            } else if (!scanner.hasNextInt()){
                System.out.println("INVALID");
                displayOptions();
                checker = false;
            }
        }
    }

    // Method to list all the filenames in a directory
    private static void listNames() {
        long start = System.nanoTime();
        Date date = new Date();
        String action = "listNames in directory";
        File folder = new File("C:\\Users\\mbkjel\\IdeaProjects\\task2\\src\\assets\\Files");
        File[] listOfFiles = folder.listFiles();

        assert listOfFiles != null;
        for (File listOfFile : listOfFiles) {
            if (listOfFile.isFile()) {
                System.out.println(listOfFile.getName());
            }
        }
        long end = System.nanoTime();
        long elapsedTime = end - start;
        logging(date, elapsedTime, action);
        fileManipulation();
    }

    // Method that prints all the files with a given extension
    private static void getFiles() {
        long start = System.nanoTime();
        Date date = new Date();
        String action = "getFiles by extension";
        Scanner scanner = new Scanner(System.in);
        System.out.println("What file extension are you looking for?");
        String word = scanner.next();

        File folder = new File("C:\\Users\\mbkjel\\IdeaProjects\\task2\\src\\assets\\Files");
        File[] files = folder.listFiles((d, name) -> name.endsWith(word));
        long end = System.nanoTime();
        long elapsedTime = end - start;
        System.out.println(Arrays.toString(files));
        logging(date, elapsedTime, action);
        fileManipulation();
    }

    /* Method to count how many times a word is being used in a file
     * This method also throws an exception if it cant reach the file
     */
    private static void countWord() {
        long start = System.nanoTime();
        Date date = new Date();
        Scanner scanner = new Scanner(System.in);
        System.out.print("What word would you like to count?");
        String word = scanner.next();
        String line;
        int counter = 0;
        File file = new File("C:\\Users\\mbkjel\\IdeaProjects\\task2\\src\\assets\\Files\\Dracula.txt");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null) {
                String[] string = line.toLowerCase().split("([,.\\s]+) ");
                for(String s : string){
                    if(s.equals(word)) {
                        counter++;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e);
        }
        System.out.println(word + " was found " + counter + " times in the text");
        long end = System.nanoTime();
        long elapsedTime = end - start;
        String action = ("search for " + word + "resulted in a " + counter + " hits");
        logging(date, elapsedTime, action);
        fileManipulation();
    }

    private static void search() {
        long start = System.nanoTime();
        Date date = new Date();
        boolean trueOrFalse = false;
        Scanner input = new Scanner(System.in);
        System.out.print("What word are you looking for?");
        String word = input.next();
        String line;
        File file = new File("C:\\Users\\mbkjel\\IdeaProjects\\task2\\src\\assets\\Files\\Dracula.txt");

        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while((line = reader.readLine()) != null) {
                String[] string = line.toLowerCase().split("([,.\\s]+) ");
                for(String s : string){
                    if (s.equals(word)) {
                        trueOrFalse = true;
                        break;
                    }
                }
                if (trueOrFalse) {
                    System.out.println(word + " was found in the text");
                } else {
                    System.out.println(word + " was not found");

                }
            }
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
        long end = System.nanoTime();
        long elapsedTime = end - start;
        String action = ("search for " + word + "resulted in a " + trueOrFalse + " result");
        logging(date, elapsedTime, action);
        fileManipulation();
    }

    // Method that count the number of lines in a document
    private static void getLines() {
        long start = System.nanoTime();
        Date date = new Date();
        String action = "getLines";
        int lines = 0;
        File file = new File("C:\\Users\\mbkjel\\IdeaProjects\\task2\\src\\assets\\Files\\Dracula.txt");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            while (reader.readLine() != null) lines++;
            reader.close();
        } catch (IOException e) {
            System.out.println("Error: " + e);
        }
        long end = System.nanoTime();
        long elapsedTime = end - start;
        System.out.println(lines);
        logging(date, elapsedTime, action);
        fileManipulation();
    }

    // Method that prints the size of the file in bytes.
    private static void getSize() {
        long start = System.nanoTime();
        Date date = new Date();
        String action = "getSize";
        File file = new File("C:\\Users\\mbkjel\\IdeaProjects\\task2\\src\\assets\\Files\\Dracula.txt");
        long bytes = file.length();
        long end = System.nanoTime();
        long elapsedTime = end - start;
        System.out.printf("%,d bytes%n", bytes);
        logging(date, elapsedTime, action);
        fileManipulation();
    }

    // Method that returns the name of the text file
    private static void getName() {
        long start = System.nanoTime();
        Date date = new Date();
        String action = "getName";
        File file = new File("C:\\Users\\mbkjel\\IdeaProjects\\task2\\src\\assets\\Files\\Dracula.txt");
        System.out.println(file.getName());
        long end = System.nanoTime();
        long elapsedTime = end - start;
        System.out.println("The function used: " + elapsedTime/1000 + " milliseconds");
        logging(date, elapsedTime, action);
        fileManipulation();
    }
}