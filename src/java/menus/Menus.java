package menus;

public class Menus {

    // Start menu, displayed at the beginning, as well as when "main menu" is called
    public static void displayOptions() {
        System.out.println("Choose one of the following operations:");
        System.out.println("1. File manipulation");
        System.out.println("2. Exit");
    }

    // Menu that is being displayed when File Manipulation is chosen in the previous menu
    public static void displayManipulation() {
        System.out.println("Choose one of the following operations:");
        System.out.println("1. List files in directory");
        System.out.println("2. Get the name of file from extension");
        System.out.println("3. Get the name of text file");
        System.out.println("4. Get the size of text file");
        System.out.println("5. Get how many lines the text file has");
        System.out.println("6. Search for specific word");
        System.out.println("7. Count a specific word");
        System.out.println("8. Go back to main Menu");
        System.out.println("9. Exit");
    }
}
