package logging;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Logging {

    /* Method that gets called each time a manipulation function is being executed.
     * @param date = The date and time when the function was called
     * @param elapsedTime = The time the function used to execute
     * @param action = What is being done
     * Adds a line to the filelog.txt file
     */
    public static void logging(Date date, loging elapsedTime, String action) {
        try {
            FileWriter writer = new FileWriter("filelog.txt", true);
            writer.write(date + " " + action + " was called and returned in " + elapsedTime + "ms\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }
}
