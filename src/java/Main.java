import java.util.Scanner;

import static menus.Menus.*;
import static manipulation.Manipulation.*;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean checker = false;
        int userInput;
        displayOptions();

        while (!checker) {
            if (scanner.hasNextInt()) {
                userInput = scanner.nextInt();
                if (userInput <= 3) {
                    switch (userInput) {
                        case 1 -> {
                            System.out.println("Heading to File Manipulation...");
                            fileManipulation();
                            checker = true;
                        }
                        case 2 -> {
                            System.out.println("Exiting program...");
                            System.exit(0);
                            checker = true;
                        }
                        default -> System.out.println("ILLEGAL INPUT: " + userInput);
                    }

                } else {
                    System.out.println("Your input is not valid, enter a number between 1-3");
                }
            } else if (!scanner.hasNextInt()){
                System.out.println("INVALID");
                displayOptions();
                checker = false;
            }
        }
    }
}